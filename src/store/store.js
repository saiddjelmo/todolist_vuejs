import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
    state: {
      todolist: [
        { done: false, text: 'To do list item one', img:'' },
        { done: false, text: 'To do list item two', img:'' },
        { done: false, text: 'To do list item five', img:'' },
        { done: true, text: 'To do list item seven', img:'' },
        { done: false, text: 'To do list item nine', img:'' },
        { done: true, text: 'Create new to do list', img:'' }
      ]
    },
    getters: {
        getToDoList: state => {
            return state.todolist.filter(function (item) {
              return item.done == false;
            });
        },
        getDoneList: state => {
          return state.todolist.filter(function (item) {
            return item.done == true;
          });
        }
    },
    mutations: {
      addNewItem: (state, item) => {
        let sameText = false;
        item.img = '';
        for(let i=0; i<state.todolist.length; i++){
          if(state.todolist[i].text === item.text) {
            sameText = true;
            break;
          }
        }

        if(!sameText) {
          state.todolist.unshift(item);
        }
        else {
          alert("You can't add same item!");
        }


      },
      deleteItem: (state, item) => {
        for(let i=0; i<state.todolist.length; i++){
          if(state.todolist[i].text === item.text) {
            state.todolist.splice(i, 1);
            break;
          }
        }
      },
      updateItem: (state, item) => {
        for(let i=0; i<state.todolist.length; i++){
          if(state.todolist[i].text === item.text) {
            state.todolist[i].img = item.img;
            break;
          }
        }
      },
      deleteDone: (state) => {
        for(let i=0; i<state.todolist.length; i++){
          if(state.todolist[i].done === true) {
            state.todolist.splice(i, 1);
            break;
          }
        }
      }

    }
});
